<?php

//определяем константу безопасности для того
//что бы при запросе данных, пользователь не получил доступа
//к другим файлам
define('VG_ACCESS', true);

header('Content-Type:text/html;charset=utf-8');
session_start();


require_once 'config.php';//тут хранятся простые базовые настройки
// для развертывания сайта на хостинге
require_once 'core/base/settings/internal_settings.php';
//хранит пути к шаблонам, и доп настройки, настройки безопасности сайта


////3 параметр 'True' позволяет функцию, загружающую класс ставить
////  в начало очереди, тем самым, позволяя разработчику
//// в любом месте добавлять свои функции-загрузчики классов
///
//spl_autoload_register('load2', true, true);

//функия показывает через дебаг,
// в какой папке по умолчанию шторм ищет файлы классов
$dir = get_include_path();

//импортируем класс исключений для функции ниже
use core\base\exceptions\RouteException;
use core\base\controller\RouteController;

try{
    RouteController::getInstance();
}
catch (RouteException $e) {

    exit($e->getMessage());
}

