<?php

//Этот контроллер отвечает по сути за весь разбор адрессной строки
namespace core\base\controller;

use core\base\exceptions\RouteException;
use core\base\settings\Settings;
use core\base\settings\ShopSettings;

//Route controller по сути, эта точка входа в систему контроллеров
class RouteController
{
    static private $_instance;

    protected $routes;

    protected $controller;
    protected $inputMethod;
    protected $outputMethod;
    protected $parameters;

    private function __clone()
    {
    }

//синглтон - способ реализации кода таким образом,
// что бы благодаря ему невозможно было создать
// больше чем 1 обьект такого же класса
    static public function getInstance()
    {
        //конструкция self означает что мы ссылаемся на наш собственный класс

        //конструкция instanceof проверяет, хранится ли  в св-ве или переменной
        //,указанной до нее обьект класса, указанный после нее

        if (self::$_instance instanceof self) {
            return self::$_instance;
        }

        return self::$_instance = new self;
    }

    private function __construct()
    {
        $adress_str = $_SERVER['REQUEST_URI'];
        //если символ / стоит в конце строки и это не явл. корнем сайта,
        // мы должны перенаправить пользователя на страницу без этого символа
        if (strrpos($adress_str, '/') === strlen($adress_str) - 1 && strrpos($adress_str, '/') !== 0) {
            //rtrim удаляет пробелы и 2 аргументом принимает символ, который так же необходимо удалить
            $this->redirect(rtrim($adress_str, '/'), 301);
        }
        //в перем-ю path мы сохр. обрез-ю строку, в которой сод-ся имя выполнения скрипта
        $path = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], 'index.php'));

        if ($path === PATH) {

            $this->routes = Settings::get('routes');

            if (!$this->routes) {
                throw new RouteException('Сайт находится на тех. обслуживании');
            }

            //ЭТОТ МЕТОД РАЗБИРАЕТ АДРЕСНУЮ СТРОКУ ПО ЗАДАННЫМ ПАРАМЕТРАМ
            if (strrpos($adress_str, $this->routes['admin']['alias']) === strlen(PATH)) {
                $url = explode('/', substr($adress_str, strlen(PATH . $this->routes['admin']['alias']) + 1));
                $plugin = array_shift($url);
                $pluginSettings = $this->routes['settings']['path'] . ucfirst($plugin . 'Settings');

                if (file_exists($_SERVER['DOCUMENT_ROOT'] . PATH . $pluginSettings . '.php')) {
                    $pluginSettings = str_replace('/', '\\', $pluginSettings);
                    $this->routes = $pluginSettings::get('routes');
                }

                //если в переменной $dir ничего не попадет, туда просто запишется /
                $dir = $this->routes['plugins']['dir'] ? '/' . $this->routes['plugins']['dir'] . '/' : '/';
                //защита от неправильной записи маршрута в настройках файла
                $dir = str_replace('//', '/', $dir);

                $this->controller = $this->routes['plugins']['path'] . $plugin . $dir;

                $hrUrl = $this->routes['plugins']['hrUrl'];

                $route = 'plugins';
                /////////////////////////////////////////////////////////////////////////////////

            } else {
                $this->controller = $this->routes ['admin']['path'];
                $hrUrl = $this->routes['admin']['hrUrl'];
                $route = 'admin';
            }
            //admin-ka
        } else {
            $url = explode('/', substr($adress_str, strlen(PATH)));

            //система будет понимать,
            // работать ли с понятными человеку ссылками в блоке пользователя
            $hrUrl = $this->routes ['user']['hrUrl'];

            $this->controller = $this->routes['user']['path'];

            $route = 'user';
        }

        $this->createRoute($route, $url);

        if ($url[1]) {
            //на 1 итерации цикла ключ у нас пуст
            $count = count($url);
            $key = '';

            if (!$hrUrl) {
                $i = 1;
            } else {
                $this->parameters['alias'] = $url[1];
                $i = 2;
            }

            for (; $i < $count; $i++) {
                if (!$key) {
                    $key = $url[$i];
                    $this->parameters[$key] = '';
                } else {
                    $this->parameters[$key] = $url[$i];
                    $key = '';
                }
            }
        } else {
            try {
                throw new \Exception('Не коректная дирректория сайта');
            } catch (\Exception $e) {
                exit($e->getMessage());
            }
        }
    }

    //контроллер будет обрабатывать адресную строку и формировать необх. массивы и св-ва
    private function createRoute($var, $arr)
    {
        $route = [];

        if (!empty($arr[0])) {
            if ($this->routes[$var]['routes'][$arr[0]]) {
                $route = explode('/', $this->routes[$var]['routes'][$arr[0]]);

                $this->controller .= ucfirst($route[0] . 'Controller');
            } else {
                $this->controller .= ucfirst($arr[0] . 'Controller');
            }
        } else {
            $this->controller .= $this->routes['default']['controller'];
        }

        $this->inputMethod = $route[1] ?: $this->routes['default']['inputMethod'];
        $this->outputMethod = $route[2] ?: $this->routes['default']['outputMethod'];
    }
}