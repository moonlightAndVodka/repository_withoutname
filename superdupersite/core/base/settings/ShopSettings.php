<?php


namespace core\base\settings;

use core\base\settings\Settings;

class ShopSettings
{
    static private $_instance;
    private $baseSettings;

    private $routes = [
        'plugins' => [
            'path' => 'core/plugins/',
            'hrUrl' => false,
            'dir' => 'controller'
        ],
    ];

    private $templateArr = [

        'text' => ['price', 'short', 'name '],
        'textarea' => ['goods_content']
    ];



    static public function get($property) {
        //метод get обр-ся к методу instance данного класса
        return self::instance()->$property;
        //метод instance возвращает св-во instance, в котором хран-ся обьект данного класса
    }

    static public function instance() {
        //если в св-ве instance хранится обьект класса, то должен его вернуть,
        // если нет - создать обьект этого класса,
        // записать его в св-во instance и вернуть его
        if(self::$_instance instanceof self) {
            return self::$_instance;
        }
        //в св-во нашего класса instance->baseSett мы сохраним ссылку на обьект класса Settings,
        // вызвав у него  метод instance
        self::$_instance = new self;
        self::$_instance->baseSettings = Settings::instance();
        $baseProperties = self::$_instance->baseSettings->clueProperties(get_class());
        self::$_instance->setProperty($baseProperties);

        return self::$_instance;
    }


    protected function setProperty($properties) {
        if ($properties) {
            foreach ($properties as $name => $property) {
                $this->$name = $property;
            }
        }
    }
        //необходимо закрыть конструктор класса
    private function  __construct()
    {
    }

    //нужно закрыть метод создания копии класса
    private function __clone()
    {
    }
}