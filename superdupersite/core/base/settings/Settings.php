<?php

namespace core\base\settings;

class Settings
{
    //т.к мы не собираемся переопределять файл настроек,
    // имеет смысл записать его в статичный класс на синглтоне
    static private $_instance;

    //private, потому что мы должны читать код внутри,
    // но не иметь возможности его перезаписывать, случайно или нет
    private $routes = [
        //настройки для админ. панели
      'admin' =>[
          //то название, по которому контроллер может понять
          // что польз-ль пытается зайти в admin панель
          'alias' => 'admin',
          'path' => 'core/admin/controller/',
          //человеку понятные ссылки, в админ. панели они не нужны
          'hrUrl'=> false
      ],
        // настройки нашего сайта
      'settings' => [
          'path' => 'core/base/settings/'
      ],
        //настройки плагинов
      'plugins' => [
          'path' => 'core/plugins/',
          'hrUrl' => false,
          'dir' => false
      ],
        //настройки для пользовательской части сайта
      'user' => [
          'path' => 'core/user/controller/',
          //человеко-понятные ссылки включены
          'hrUrl' => true,
          //маршруты
          'routes' => [
            'catalog' =>'site'
          ]
      ],
        //раздел "по умолчанию". Когда неопределился какой то метод
      'default' => [
          //контроллер подлючаемый по умолчанию
          'controller' => 'IndexController',
          //метод по умолчанию, вызываемый у контроллера,
          // если не определился метод вызова
          'inputMethod' => 'inputData',
          //вывод данных в пользовательскую часть по умолчанию
          'outputMethod' => 'outputData'
        ]
    ];

    private $templateArr = [

        'text' => [ 'name', 'phone', 'address'],
        'textarea' => ['content', 'keywords']
    ];

    private $lalala = 'lalala';

    //необходимо закрыть конструктор класса
    private function  __construct()
    {
    }

    //нужно закрыть метод создания копии класса
    private function __clone()
    {
    }

    //нужно создать публичный стат. метод, создающий объект данного класса
    static public function get($property) {
        //метод get обр-ся к методу instance данного класса
        return self::instance()->$property;
        //метод instance возвращает св-во instance, в котором хран-ся обьект данного класса
    }

    static public function instance() {
        //если в св-ве instance хранится обьект класса, то должен его вернуть,
        // если нет - создать обьект этого класса,
        // записать его в св-во instance и вернуть его
        if(self::$_instance instanceof self) {
            return self::$_instance;
        }

        return self::$_instance = new self;
    }

    public function clueProperties($class)
    {
        $baseProperties = [];

        foreach ($this as $name => $item) {
            $property = $class::get($name);
            if (is_array($property) && is_array($item)) {
                //фнкц-я, которая обрабатывает многомерные массивы
                $baseProperties[$name] = $this->arrayMergeRecursive($this->$name, $property);
                continue;
            }
            //если в проперти null/false и тд, мы берем результирующий массив baseProp..
            // и в его ячейку имени записываем то, что  находится
            //B св-ве основного обьекта настроек.
            if (!$property) $baseProperties[$name] = $this->$name;
        }

        return $baseProperties;
    }

    //этот метод приклеивает все дочернии настройки к основным.
    public function arrayMergeRecursive () {
        $arrays = func_get_args();
        $base = array_shift($arrays);

        foreach ($arrays as $array) {
            foreach ($array as $key => $value) {
                if (is_array($value) && is_array($base[$key])) {
                    $base[$key] = $this->arrayMergeRecursive($base[$key], $value);
                } else {
                    if (is_int($key)){
                        //если эт нумерованный массив, мы должны перейти на след. итерацию этого цикла
                        if(!in_array($value,$base)) array_push($base, $value);
                        continue;
                    }
                    $base[$key] = $value;
                }
            }
        }
        return $base;
    }
}